#include "porting_layer.h"
//liteos_a now is not supporting shm_open interface

int main(int argc, char **argv)
{
    DECLARE_TIME_COUNTERS(no_time_t, _)
    
    if (fork() == 0)
    {
        DECLARE_TIME_STATS(int64_t)

        for (int i = 0; i < 1000; i++)
        {
          WRITE_T1_COUNTER(_)
          no_task_yield();
          WRITE_T2_COUNTER(_)
          COMPUTE_TIME_STATS(_, i)
        }
        REPORT_BENCHMARK_RESULTS("-- process context switch 2 muti-results --")

        exit(0);
        
    }else {
        for (int i = 0; i < 1000; i++)
        {
          no_task_yield();
        }
        exit(0);
      
    }



}