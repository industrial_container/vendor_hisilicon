# RECORD

1. round_robin
   + recording the time of pthread switching to measure the performance of system
   + Question:
     + The scheder of SCHED_RR or SCHED_FIFO which the pthread set will get the same value in the thread of SCHED_RR, because the scheder is set in pthread_attr_create by system.
     + The thread priority must be set 31, or the sequence of sched is error, the created thread will run before all the child thread has been created

   + liteos_a result:
      ```
      setting up affinity 0
      setting up prio 18
      -- round robin context switch results --
      max=564680
      min=11520
      average=16726
      0000000000123451122334455...11223344551122334455112233445512345
      ```

2. preempt
   + liteos_A
    + results
      ```
      OHOS:/$ ./bin/preempt                                                          
      setting up affinity 0
      setting up prio 20
      0222231Time difference: 488940
      ```