# RECORD

**Attention**: you should make sure to destroy the message queue when you want to create a new message queue with a same name.Maybe some previous bugs i encounter are caused by the suddenly break process running without correct recycled of message queue.

1. mq
   get the delay of signal transmission and reception.
   + linux:
     when you run the code in linux with no change of code ,  you will get the result as follows.
     + result
          ```
          setting up affinity 0
          setting up prio 21
          -- MQ: Receive block --
          max=3670
          min=-4229
          average=-738
          -- MQ: Signal unblock --
          max=13065
          min=1712
          average=3859
          0300222223333332222223333332222223333332222243335554444445555554444445555554444445555540
          ```

     if you want to get correct record, you need to change the priority of thread, make the receive thread priority bigger than send receive.

   + liteos_a:
     By ddefault, it can get correct result.
     + result
          ```
          OHOS:/$ ./bin/mq                                                               
          setting up affinity 0
          setting up prio 20
          -- MQ: Receive block --
          max=186340
          min=32760
          average=49918
          -- MQ: Signal unblock --
          max=453020
          min=38460
          average=87748

          0300233223322332233223322332233223322332233223524554455445544554455445544554455445544540
          ```


2. mq_workload
   + liteos_a:
     + result
          ```
          OHOS:/$ ./bin/mq_workload                                                      
          setting up affinity 0
          setting up prio 20
          -- MQ workload --
          max=1012260
          min=51640
          average=125252
          030022330111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111444444444444444444444444442233444444444444444444444422332233223322332233223322332233223322332233223322332233223322332233223322332233....2233223322332233223322332233223322332233223322332233223322332233223322332233223322332233223322332233223322332233223322332233223322322
          ```

   + linux:
     + result
          ```
          setting up affinity 0
          setting up prio 38
          -- MQ workload --
          max=32648
          min=2163
          average=4484
          ```

3. mq_processing
   + linux
     + result
          ```
          setting up affinity 0
          setting up prio 20
          --- MQ Send ---
          max=46104
          min=578
          average=1889
          -- MQ Receive --
          max=26877
          min=584
          average=771
          Test end!
          ```

   + liteos_a
     + result
          ```
          setting up affinity 0
          setting up prio 20
          --- MQ Send ---
          max=5221520
          min=20900
          average=26498
          -- MQ Receive --
          max=1332040
          min=21400
          average=30854
          Test end!
          ```