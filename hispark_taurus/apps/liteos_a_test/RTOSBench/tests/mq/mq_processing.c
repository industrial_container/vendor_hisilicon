/* Copyright (c) 2019, Guillaume Champagne
 * All rights reserved.
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

#include "porting_layer.h"

#define NB_ITER 1000000
#define MQ_LEN 1
#define MQ_MSGSIZE 50
#define MQ_DEPTH 50

#define MQ_NAME "/12345"
#define MQ_MSGSEND "abced"

no_task_retval_t mq_initialize_test(no_task_argument_t args);
no_task_retval_t task(no_task_argument_t args);

no_task_handle_t task_handle;
no_mq_t mq;

no_main_retval_t main(no_main_argument_t args)
{
	no_initialize_test(mq_initialize_test);
	return MAIN_DEFAULT_RETURN;
}

no_task_retval_t mq_initialize_test(no_task_argument_t args)
{
	/* Create resources */
	no_mq_destory(MQ_NAME);
	no_mq_create(&mq, MQ_NAME, MQ_DEPTH, MQ_MSGSIZE);

	task_handle = no_create_task(task, NULL, LITE_PRIO);

	no_join_task(task_handle);

	no_serial_write("\nTest end!\n");
	no_mq_destory(MQ_NAME);

	return TASK_DEFAULT_RETURN;
}

no_task_retval_t task(no_task_argument_t args)
{
	int32_t i;
	DECLARE_TIME_COUNTERS(no_time_t, send)
	DECLARE_TIME_COUNTERS(no_time_t, recv)
	DECLARE_TIME_STATS(int64_t)

	/* 1a - Measure MQ send . */
	for (i = 0; i < NB_ITER; i++)
	{
		WRITE_T1_COUNTER(send)
		no_mq_send(mq, MQ_MSGSEND);
		WRITE_T2_COUNTER(send)
		no_mq_receive(mq);
		COMPUTE_TIME_STATS(send, i);
	}

	REPORT_BENCHMARK_RESULTS("--- MQ Send ---");
	RESET_TIME_STATS();

	/* 1a - Measure MQ receive. */
	for (i = 0; i < NB_ITER; i++)
	{
		no_mq_send(mq, MQ_MSGSEND);
		WRITE_T1_COUNTER(recv)
		no_mq_receive(mq);
		WRITE_T2_COUNTER(recv)
		COMPUTE_TIME_STATS(recv, i);
	}

	REPORT_BENCHMARK_RESULTS("-- MQ Receive --")

	no_task_suspend_self();

	return TASK_DEFAULT_RETURN;
}
